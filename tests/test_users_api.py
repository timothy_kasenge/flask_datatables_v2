#!/usr/bin/env python
import unittest
from app import create_app, db
from flask_test_helpers import FlackTestCase
from app.auth_token import generate_token


class TestCase(FlackTestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.ctx = self.app.app_context()
        self.ctx.push()
        db.drop_all()  # just in case
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.ctx.pop()

    def test_users(self):
        # get users without auth
        r, s, h = self.get('/api/users')
        self.assertEqual(s, 401)

        # get users with bad auth
        r, s, h = self.get('/api/users', token_auth='bad-token')
        self.assertEqual(s, 401)

        # create a new user
        r, s, h = self.post(
            '/api/users', data={"username": "tomy",
                                "password": "password",
                                "email": "mai@mail.com"})
        self.assertEqual(s, 201)

        # create a duplicate user
        r, s, h = self.post('/api/users', data={"username": "tomy",
                                                "password": "password",
                                                "email": "mai@mail.com"})
        self.assertEqual(s, 400)

        # create an incomplete user
        r, s, h = self.post(
            '/api/users', data={'username': 'foo1'})
        self.assertEqual(s, 400)


if __name__ == '__main__':
    unittest.main(verbosity=2)
