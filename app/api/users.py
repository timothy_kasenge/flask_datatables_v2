from app import db
from app.api import bp
from flask import jsonify, request, abort, _request_ctx_stack, current_app, url_for as _url_for
from app.models import User
from app.datatables import Datatable
from app.api.auth import token_auth
import time


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time.time())


def url_for(*args, **kwargs):
    """url_for replacement that works even when there is no request context.
    """
    if '_external' not in kwargs:
        kwargs['_external'] = False
    reqctx = _request_ctx_stack.top
    if reqctx is None:
        if kwargs['_external']:
            raise RuntimeError('Cannot generate external URLs without a '
                               'request context.')
        with current_app.test_request_context():
            return _url_for(*args, **kwargs)
    return _url_for(*args, **kwargs)


@bp.route('/users/<int:id>', methods=['GET'])
@token_auth.login_required
def get_user(id):
    displayable_columns = ['username', 'email']
    user = Datatable(User).getRecord(displayable_columns, id)

    return jsonify({"error": False, "message": user})


@bp.route('/users', methods=['GET'])
@token_auth.login_required
def get_users():
    displayable_columns = ['username', 'email']
    user_data = Datatable(User, 'api.get_users').getRecords(
        displayable_columns, request)
    r = user_data
    r.status_code = 201
    return r


@bp.route('/users', methods=['POST'])
def create_user():

    user = User()
    req_data = request.get_json()

    if not 'username' in req_data or not 'email' in req_data or not 'password' in req_data:
        abort(400)

    if user.check_user_exist(req_data):
        abort(400)

    user.from_dict(req_data, new_user=True)
    db.session.add(user)
    db.session.commit()
    r = jsonify({"error": False, "message": user.to_dict()})
    r.status_code = 201
    r.headers['Location'] = url_for('api.get_user', id=user.id)
    return r


@bp.route('/users/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_user(id):
    req_data = request.get_json()

    displayable_columns = ['username', 'email']
    user_data = Datatable(User).update(displayable_columns, req_data, id)

    r = jsonify({"error": False, "message": user_data})
    r.status_code = 201
    return r
