from flask import jsonify, g, request
from app import db
from app.api import bp
from app.api.auth import basic_auth
from app.api.auth import token_auth
from app.auth_token import generate_token


@bp.route('/tokens', methods=['POST'])
@basic_auth.login_required
#  the database to store tokens
def get_token():
    token = g.current_user.get_token()
    db.session.commit()
    results = {'token': token}
    response = jsonify(results)
    response.status_code = 200
    return response

# jwt
# def new_token():
#     """
#     Generate an access token for the user.
#     This endpoint is requires basic auth with nickname and password.
#     """
#     return jsonify({'token': generate_token(g.current_user.id)})


@bp.route('/tokens', methods=['DELETE'])
@token_auth.login_required
# using a saved token from the database
def revoke_token():
    g.current_user.revoke_token()
    db.session.commit()
    return '', 204
# using jwt
# @token_auth.login_required
# def revoke_token():
#     """
#     Revoke a user token.
#     This endpoint is requires a valid user token.
#     """
#     # get the token from the Authorization header
#     token = request.headers['Authorization'].split()[1]

#     # you will need to create a revoke tokens table to save the tokens that have been revoked and check if they exist there before proceeding

#     return '', 204
